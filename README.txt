Extrafield Settings
-------------------

This module provides the ability for module creators and developers
to create field-formatter-like settings forms for extra_fields created
with hook_field_extra_fields().

Configuration
-------------

There is no UI configuration form for this module. To use this module a
settings form callback and optionally a render callback must be defined
for your custom extra_fields. See the example below:

  function hook_field_extra_fields() {
    $extra['entity_type']['bundle'] = array(
      'display' => array(
        'field_name' => array(
          'label' => 'Field Name',
          'weight' => 0,
          //
          // Below are the callbacks for extrafield_settings
          'callback' => 'example_field_name_render',
          'settings callback' => 'example_field_name_settings_form'
        ),
      ),
    );
    return $extra;
  }

  function example_field_name_settings_form($settings) {
    return array(
      'link_text' => array(
        '#type' => 'textfield',
        '#title' => isset($settings['link_text']) ?
          $settings['link_text'] : t('Click Here'),
        '#description' => t('Link text for my custom button'),
      ),
    );
  }

  function example_field_name_render($entity, $type, $view_mode, $settings) {
    return array(
      '#theme' => 'link',
      '#title' => isset($settings['link_text']) ?
        $settings['link_text'] : t('Click here'),
      '#path' => entity_uri($type, $entity),
      '#options' => array(
        'attributes' => array(),
        'html' => FALSE,
      ),
    );
  }
