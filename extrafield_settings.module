<?php

/**
 * @file
 * Core hooks for the Extrafield settings module.
 *
 * Allows developers to implement field-formatter-like
 * settings forms for extra_fields.
 */

/**
 * Implements hook_entity_view().
 *
 * Add visible extra_fields to the entity with settings for rendering.
 */
function extrafield_settings_entity_view($entity, $type, $view_mode, $langcode) {
  $bundle = isset($entity->bundle) ? $entity->bundle : (isset($entity->type) ? $entity->type : NULL);

  $extra = field_info_extra_fields($type, $bundle, 'display');
  if (empty($extra)) {
    return;
  }

  foreach ($extra as $name => $field) {
    if (!$field['display'][$view_mode]['visible']) {
      continue;
    }

    if (!isset($field['callback']) || !function_exists($field['callback'])) {
      continue;
    }

    $settings = isset($field['display'][$view_mode]['settings']) ?
      $field['display'][$view_mode]['settings'] :
      array();

    $entity->content[$name] = $field['callback']($entity, $type, $view_mode, $settings);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Modify the Field UI display (Manage Display) form
 * and add settings for extra_fields.
 *
 * @see field_ui_display_overview_form()
 */
function extrafield_settings_form_field_ui_display_overview_form_alter(&$form, &$form_state) {
  if (empty($form['#extra'])) {
    return;
  }

  // Visible extra_fields may not have settings. If we don't add settings
  // forms here we don't need to call our form submit handler.
  $settings_added = FALSE;

  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $view_mode = $form['#view_mode'];

  $extra = field_info_extra_fields($entity_type, $bundle, 'display');
  $bundle_settings = field_bundle_settings($entity_type, $bundle);

  foreach ($form['#extra'] as $name) {
    $field = $extra[$name];

    if (!isset($field['settings callback'])) {
      continue;
    }

    $row = &$form['fields'][$name];

    $settings = isset($form_state['formatter_settings'][$name]) ? $form_state['formatter_settings'][$name] : array();

    $stored_settings = isset($bundle_settings['extra_fields']['display'][$name][$view_mode]['settings'])
      ? $bundle_settings['extra_fields']['display'][$name][$view_mode]['settings']
      : array();

    $settings += $stored_settings;

    // Base button element for the various formatter settings actions.
    $base_button = array(
      '#submit' => array('field_ui_display_overview_multistep_submit'),
      '#ajax' => array(
        'callback' => 'field_ui_display_overview_multistep_js',
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
      ),
      '#field_name' => $name,
    );

    if ($form_state['formatter_settings_edit'] == $name) {
      $row['format']['settings_edit_form'] = array();
      $settings_form = array();

      $function = $field['settings callback'];
      if (function_exists($function)) {
        $settings_form = $function($settings);
      }

      if (!$settings_form) {
        continue;
      }
      $row['format']['#cell_attributes'] = array('colspan' => 3);
      $row['format']['settings_edit_form'] = array(
        '#type' => 'container',
        '#attributes' => array('class' => array('field-formatter-settings-edit-form')),
        '#parents' => array('fields', $name, 'settings_edit_form'),
        'label' => array(
          '#markup' => t('Display settings:'),
        ),
        'settings' => $settings_form,
        'actions' => array(
          '#type' => 'actions',
          'save_settings' => $base_button + array(
            '#type' => 'submit',
            '#name' => $name . '_formatter_settings_update',
            '#value' => t('Update'),
            '#op' => 'update',
          ),
          'cancel_settings' => $base_button + array(
            '#type' => 'submit',
            '#name' => $name . '_formatter_settings_cancel',
            '#value' => t('Cancel'),
            '#op' => 'cancel',
            // Do not check errors for the 'Cancel' button, but make sure we
            // get the value of the 'formatter type' select.
            '#limit_validation_errors' => array(array('fields', $name, 'type')),
          ),
        ),
      );
      $row['#attributes']['class'][] = 'field-formatter-settings-editing';
    }
    else {
      $row['settings_edit'] = $base_button + array(
        '#type' => 'image_button',
        '#attributes' => array('class' => array('field-formatter-settings-edit')),
        '#src' => '/misc/configure.png',
        '#name' => $name . '_settings_edit',
        '#op' => 'edit',
        '#prefix' => '<div class="field-formatter-settings-edit-wrapper">',
        '#suffix' => '</div>',
      );
    }

    $settings_added = TRUE;
  }

  if ($settings_added) {
    $form['#submit'][] = 'extrafield_settings_form_field_ui_display_overview_form_submit';
  }
}

/**
 * Implements submit handler for Field UI display (Manage Display) form.
 *
 * Save custom extra_field settings to field_bundle_settings.
 *
 * @see field_ui_display_overview_form_submit()
 */
function extrafield_settings_form_field_ui_display_overview_form_submit($form, &$form_state) {
  if (empty($form['#extra'])) {
    return;
  }

  $form_values = $form_state['values'];
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $view_mode = $form['#view_mode'];

  $bundle_settings = field_bundle_settings($entity_type, $bundle);

  foreach ($form['#extra'] as $name) {
    $settings = array();
    if (isset($form_values['fields'][$name]['settings_edit_form']['settings'])) {
      $settings = $form_values['fields'][$name]['settings_edit_form']['settings'];
    }
    elseif (isset($form_state['formatter_settings'][$name])) {
      $settings = $form_state['formatter_settings'][$name];
    }

    if ($settings) {
      $bundle_settings['extra_fields']['display'][$name][$view_mode]['settings'] = $settings;
    }
  }

  field_bundle_settings($entity_type, $bundle, $bundle_settings);
}

/**
 * Implements hook_help().
 */
function extrafield_settings_help($path, $arg) {
  switch ($path) {
    case 'admin/help#extrafield_settings':
      $output = '';
      $output .= '<h3>About</h3>';
      $output .= '<p>This module provides the ability for module creators and developers to create field-formatter-like settings forms for extra_fields created with hook_field_extra_fields().</p>';
      $output .= '<h3>Configuration</h3>';
      $output .= '<p>There is no UI configuration form for this module. To use this module a settings form callback and optionally a render callback must be defined for your custom extra_fields. For more information, please see the README in the module directory.</p>';
      return $output;
    break;

  }
}
